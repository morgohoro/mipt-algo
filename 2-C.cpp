#include <iostream>
#include <climits>
#include <cmath>
#include <queue>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::floor;
using std::max;
using std::min;
using std::queue;
using std::string;
using std::vector;

const char separator_0 = '\0'; // separator between two input strings
const char separator_1 = '\1'; // separator at the end of string_structures str
const int alphabet_size = 256;

class Sparse_table {
public:
    Sparse_table() = default;
    Sparse_table(const vector<int> &lcp);
    int query(const int left, const int right) const;
    bool empty() const;
private:
    vector<vector<int>> sparse_table;
};

Sparse_table::Sparse_table(const vector<int> &lcp) {
    vector<int> row(floor(log2(lcp.size()) + 1));
    for (uint32_t i = 0; i < lcp.size(); ++i) {
        sparse_table.push_back(row);
        sparse_table[i][0] = lcp[i];
    }
    for (uint32_t power = 1; power < row.size(); ++power) {
        for (uint32_t i = 0; (i + (1 << power) - 1) < lcp.size(); ++i) {
            const int right = sparse_table[i + (1 << (power - 1))][power - 1];
            const int left = sparse_table[i][power - 1];
            if (left < right) {
                sparse_table[i][power] = left;
            }
            else {
                sparse_table[i][power] = right;
            }
        }
    }
}

int Sparse_table::query(const int left, const int right) const {
    const int power = floor(log2(right - left + 1));
    const int left_min = sparse_table[left][power];
    const int right_min = sparse_table[right - (1 << power) + 1][power];
    if (left_min <= right_min) {
        return left_min;
    }
    else {
        return right_min;
    }
}

bool Sparse_table::empty() const {
    return sparse_table.empty();
}

// contains string, suffix array and lcp
class String_structures {
public:
    String_structures(const string &text) : str(text + separator_1) {};
    string find_substring(uint64_t k_common_sub, 
                          uint32_t len_1, uint32_t len_2);
    vector<int> get_lcp();
    vector<int> get_suffix_arr();
    size_t size() const;
private:
    const string str;
    vector<int> suffix_arr;
    vector<int> lcp;
    Sparse_table lcp_sparse_t;
    void calc_positions(vector<int> &insertion_indices);
    void calc_suffix_arr();
    void calc_lcp();
    void calc_sparse_table();
    int lcp_query(int left, int right);
};

vector<int> String_structures::get_lcp() {
    if (suffix_arr.size() == 0) {
        calc_suffix_arr();
    }
    if (lcp.size() == 0) {
        calc_lcp();
    }
    vector<int> arr = vector<int>();
    arr.insert(arr.begin(), lcp.begin(), lcp.end());
    return arr;
}

vector<int> String_structures::get_suffix_arr() {
    if (suffix_arr.size() == 0) {
        calc_suffix_arr();
    }
    vector<int> arr = vector<int>();
    arr.insert(arr.begin(), suffix_arr.begin(), suffix_arr.end());
    return arr;
}

size_t String_structures::size() const {
    return str.length();
}

void String_structures::calc_positions(vector<int> &insertion_indices) {
    for (uint32_t i = 1; i < insertion_indices.size() - 1; ++i) {
        insertion_indices[i + 1] += insertion_indices[i];
    }
}

void String_structures::calc_suffix_arr() {
    // first step - sort by first letter
    // calc the number of each letter
    vector<int> insertion_indices(alphabet_size, 0);
    for (char ch : str) {
        ++insertion_indices[ch + 1];
    }
    // convert to vector with the insertion indices
    calc_positions(insertion_indices);
    suffix_arr.resize(str.length(), 0);
    // counting sort
    for (uint32_t index = 0; index < str.length(); ++index) {
        suffix_arr[insertion_indices[str[index]]++] = index;
    }
    // calc equivalence classes of the sorted substrings
    vector<int> equiv_classes(str.length()); // equivalence classes
    int class_number = 0;
    char last_char = separator_1;
    for (int suffix : suffix_arr) {
        if (str[suffix] != last_char) {
            last_char = str[suffix];
            ++class_number;
        }
        equiv_classes[suffix] = class_number;
    }
    // first step completed
    uint32_t cur_len = 1; // current length of sorted part
    // next step - sort substrings with length 2 * cur_len
    while (cur_len <= str.length()) {
        // suffixes sorted by the second half of substrings of len "cur_len * 2"
        vector<int> sorted_by_second_part(str.length());
        // sort by the second half
        for (uint32_t i = 0; i < str.length(); ++i) {
            int suf = (suffix_arr[i] + str.length() - cur_len) % str.length();
            sorted_by_second_part[i] = suf;
        }
        insertion_indices.clear();
        insertion_indices.resize(str.length() + alphabet_size, 0);
        // sort by the first half
        // sort is stable, so it means we'll get full sorted substrings
        for (int suf : sorted_by_second_part) {
            ++insertion_indices[equiv_classes[suf] + 1]; // "+ 1" because of sep
        }
        calc_positions(insertion_indices);
        // counting sort
        for (uint32_t i = 0; i < suffix_arr.size(); ++i) {
            const int index = sorted_by_second_part[i];
            suffix_arr[insertion_indices[equiv_classes[index]]++] = index;
        }
        // assign new equivalence classes
        vector<int> new_equiv_classes(str.length());
        class_number = 0;
        for (uint32_t i = 1; i < suffix_arr.size(); ++i) {
            const int suf_1 = (suffix_arr[i] + cur_len) % str.length();
            const int suf_2 = (suffix_arr[i - 1] + cur_len) % str.length();
            const int class_1 = equiv_classes[suffix_arr[i]];
            const int class_2 = equiv_classes[suffix_arr[i - 1]];
            const int equiv_1 = equiv_classes[suf_1];
            const int equiv_2 = equiv_classes[suf_2];
            if (class_1 != class_2 or equiv_1 != equiv_2) {
                ++class_number;
            }
            new_equiv_classes[suffix_arr[i]] = class_number;
        }
        equiv_classes = new_equiv_classes;
        cur_len *= 2;
    }
}

void String_structures::calc_lcp()
{
    lcp.resize(str.length(), 0);
    vector<uint32_t> rank(str.length(), 0); // suffix_arr^(-1)
    for (uint32_t i = 0; i < str.length(); ++i) {
        rank[suffix_arr[i]] = i;
    }
    // first step - calc lcp for rank[0]
    // next - iterate over suffixes from the biggest to the smallest
    // for each suffix calc lcp[rank[i]], starting from lcp[rank[i - 1]] - 1
    for (uint32_t i = 0, cur_prefix = 0; i < str.length(); ++i) {
        if (cur_prefix > 0) {
            --cur_prefix;
        }
        if (rank[i] == str.length() - 1) {
            lcp[str.length() - 1] = -1;
            cur_prefix = 0;
            continue;
        }
        int cur_suf = suffix_arr[rank[i] + 1];
        uint32_t cur_ind_1 = i + cur_prefix;
        uint32_t cur_ind_2 = cur_suf + cur_prefix;
        uint32_t max_ind = max(cur_ind_1, cur_ind_2);
        while (max_ind++ < str.length() and 
               str[cur_ind_1++] == str[cur_ind_2++]) {
            ++cur_prefix;
        }
        lcp[rank[i]] = cur_prefix;
    }
}

void String_structures::calc_sparse_table() {
    lcp_sparse_t = Sparse_table(lcp);
}

int String_structures::lcp_query(int left, int right) {
    return lcp_sparse_t.query(left, right);
}

string String_structures::find_substring(uint64_t k_common_sub, 
                                     uint32_t len_1, uint32_t len_2) {
    if (suffix_arr.size() == 0) {
        calc_suffix_arr();
    }
    if (lcp.size() == 0) {
        calc_lcp();
    }
    if (lcp_sparse_t.empty()) {
        calc_sparse_table();
    }
    string substr = "";
    uint64_t cur_k = 0;
    queue<int> suf_1, suf_2;
    uint32_t ind = 0;
    while (ind < suffix_arr.size()) {
        if (suffix_arr[ind] > static_cast<int>(len_1)) {
            suf_2.push(ind++);
        }
        else {
            suf_1.push(ind++);
        }
    }
    int prev_lcp = 0;
    int last_suf_1, last_suf_2;
    while (cur_k < k_common_sub and !suf_1.empty() and !suf_2.empty()) {
        last_suf_1 = suf_1.front();
        last_suf_2 = suf_2.front();
        int min_lcp = INT_MAX;
        const int begin = min(last_suf_1, last_suf_2);
        const int end = max(last_suf_1, last_suf_2);
        min_lcp = lcp_query(begin, end - 1);
        cur_k += max(0, min_lcp - prev_lcp);
        prev_lcp = min_lcp;
        const char next_char_1 = str[suffix_arr[last_suf_1] + min_lcp];
        const char next_char_2 = str[suffix_arr[last_suf_2] + min_lcp];
        if (next_char_1 > next_char_2) {
            suf_2.pop();
        }
        else {
            suf_1.pop();
        }
    }
    if (cur_k >= k_common_sub) {
        for (uint32_t i = 0; i < prev_lcp - (cur_k - k_common_sub); ++i) {
            substr += str[suffix_arr[last_suf_1] + i];
        }
    }
    return substr;
}

int main()
{
    uint64_t k_common_sub;
    string str_1, str_2;
    cin >> str_1 >> str_2 >> k_common_sub;
    String_structures s_struct(str_1 + separator_0 + str_2);
    const uint32_t len_1 = str_1.length();
    const uint32_t len_2 = str_2.length();
    string str = s_struct.find_substring(k_common_sub, len_1, len_2);
    if (str.empty()) {
        cout << -1;
    }
    else {
        cout << str;
    }
    return 0;
}
