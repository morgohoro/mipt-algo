#include <algorithm>
#include <cmath>
#include <iostream>
#include <vector>

using std::abs;
using std::cin;
using std::cout;
using std::istream;
using std::ostream;
using std::vector;

const double eps = 1e-8;

struct Vector;
struct Point;

class Polygon {
public:
    Polygon() = default;
    void add_new_point(const Point &point);
    // add new point with coords of previous point + vector's coords
    void add_new_point_by_vector(const Vector &vect);
    void reverse(); // reverse points vector
    void reflect(); // reflect all points relative to the origin
    Point operator[](const uint32_t index) const;
    uint32_t size() const;
    bool check_origin(); // check if origin within polygon

    friend istream &operator>>(istream &in, Polygon &pol);
    friend uint32_t find_min_point(const Polygon &pol);
    friend Polygon build_minkowski_sum(const Polygon &pol1, 
                                       const Polygon &pol2);
private:
    vector<Point> points;
};


struct Point {
public:
    Point(double x, double y) : x(x), y(y) {};
    Point() = default;
    bool operator<(const Point &point) const;

    double x;
    double y;
};

bool Point::operator<(const Point &point) const { 
    return y < point.y or y == point.y and x < point.x; 
}

istream &operator>>(istream &in, Point &point) {
    double x, y;
    in >> x >> y;
    point = Point(x, y);
    return in;
}

ostream &operator<<(ostream &out, const Point &point) {
    out << point.x << ' ' << point.y << '\n';
    return out;
}

struct Vector {
public:
    Vector(double x, double y) : x(x), y(y) {};
    Vector() = default;

    double x;
    double y;
};

Vector operator-(const Point &point0, const Point &point1) {
    return Vector(point0.x - point1.x, point0.y - point1.y);
}

double dot(const Vector &vect1, const Vector &vect2) {
    return vect1.x * vect2.x + vect1.y * vect2.y;
}

double norm(const Vector &vect) {
    return sqrt(dot(vect, vect));
}

Vector operator*(double multiplier, const Vector &vect) {
    return Vector(multiplier * vect.x, multiplier * vect.y);
}

Vector operator/(const Vector &vect, double divider) {
    return Vector(vect.x / divider, vect.y / divider);
}

Vector operator+(const Vector &vect1, const Vector &vect2) {
    return Vector(vect1.x + vect2.x, vect1.y + vect2.y);
}

Vector operator-(const Vector &vect1, const Vector &vect2) {
    return Vector(vect1.x - vect2.x, vect1.y - vect2.y);
}

Point operator+(const Point &point, const Vector &vect) {
    return Point(point.x + vect.x, point.y + vect.y);
}

double operator*(const Vector &vect1, const Vector &vect2) {
    return vect1.x * vect2.y - vect1.y * vect2.x;
}

void Polygon::add_new_point(const Point &p) {
    points.push_back(p);
}

void Polygon::add_new_point_by_vector(const Vector &v) {
    points.push_back(points[points.size() - 1] + v);
}

void Polygon::reverse() {
    std::reverse(points.begin(), points.end());
}

void Polygon::reflect() {
    for (uint32_t i = 0; i < points.size(); ++i) {
        points[i] = Point(-points[i].x, -points[i].y);
    }
}

uint32_t Polygon::size() const {
    return points.size();
}

Point Polygon::operator[](uint32_t index) const {
    return points[index];
}

bool Polygon::check_origin() {
    bool intersect_odd = false;
    uint32_t size = this->size();
    double x = 0.0;
    double y = 0.0;
    for (uint32_t i = 0, j = size - 1; i < size; j = i++) {
        double tangent_a = (points[j].x - points[i].x) / (points[j].y - points[i].y);
        if ((((points[i].y <= y) and (points[j].y > y)) or 
            ((points[j].y <= y) and (points[i].y > y))) and 
            (x > tangent_a * (y - points[i].y) + points[i].x)) {
            intersect_odd = !intersect_odd;
        }
    }
    return intersect_odd;
}

istream &operator>>(istream &in, Polygon &pol) {
    uint32_t points_num;
    cin >> points_num;
    for (uint32_t i = 0; i < points_num; ++i) {
        Point point;
        cin >> point;
        pol.add_new_point(point);
    }
    pol.reverse();
    return in;
}

uint32_t find_min_point(const Polygon &pol) {
    uint32_t min_point;
    for (uint32_t k = 0; k < pol.size(); ++k) {
        if (pol[k] < pol[min_point]) {
            min_point = k;
        }
    }
}

Polygon build_minkowski_sum(Polygon &pol1, Polygon &pol2) {
    const uint32_t size_1 = pol1.size();
    const uint32_t size_2 = pol2.size();
    vector<bool> used1(size_1, false);
    vector<bool> used2(size_2, false);
    pol1.add_new_point(pol1[0]);
    pol2.add_new_point(pol2[0]);
    Polygon  new_pol;
    uint32_t i = 0, j = 0; // min point in first and second polygons
    i = find_min_point(pol1);
    j = find_min_point(pol2);
    new_pol.add_new_point(pol1[i] + (pol2[j] - Point(0.0, 0.0)));
    while(!(used1[i] and used2[j])) {
        const Vector vect1 = pol1[i + 1] - pol1[i];
        const Vector vect2 = pol2[j + 1] - pol2[j];
        if (used1[i] or !used2[j] and vect1 * vect2 < -eps) {
            new_pol.add_new_point_by_vector(vect2);
            used2[j] = true;
            ++j;
        }
        else {
            if (used1[i] or !used2[j] and vect1 * vect2 > eps) {
                new_pol.add_new_point_by_vector(vect1);
                used1[i] = true;
                ++i;
            }
            else {
                new_pol.add_new_point_by_vector(vect2 + vect1);
                used1[i] = true;
                used2[j] = true;
                ++i;
                ++j;
            }
        }
        if (i == size_1) {
            i = 0;
        }
        if (j == size_2) {
            j = 0;
        }
    }
    return new_pol;
}

int main() {
    Polygon pol1;
    cin >> pol1;
    Polygon pol2;
    cin >> pol2;
    pol2.reflect();
    Polygon minkowski_sum = build_minkowski_sum(pol1, pol2);
    if (minkowski_sum.check_origin()) {
        cout << "YES";
    }
    else {
        cout << "NO";
    }
    return 0;
}