#include <cassert>
#include <iostream>
#include <limits>
#include <string>
#include <vector>

using std::cin;
using std::cout;
using std::max;
using std::min;
using std::numeric_limits;
using std::ostream;
using std::string;
using std::vector;

// inf means that edge of node of suffix tree continues to the end of the string
const uint32_t inf = numeric_limits<uint32_t>::max();
const uint8_t alphabet_size = 127;

class Node {
public:
    Node() = default;
    Node(const uint32_t first, const uint32_t last) : 
        begin(first), end(last), 
        sons(alphabet_size, 0) {};
    // zero as a son means that this is root, but because root can't have parent
    // having zero as a son means that there is no son by this edge
    uint32_t get_son(const char letter) const; // return index of son or 0
    void set_son(const char letter, const uint32_t son); // add son by edge
    uint32_t get_suffix_link() const; // return index of node-suffix-link or 0
    void set_suffix_link(const uint32_t link); // set suffix link
    // return length of edge
    uint32_t get_edge_len(const uint32_t cur_position) const;
    uint32_t get_edge_start() const; // return index of string where edge starts
    // return index of string next to that where edge ends
    uint32_t get_edge_end() const; 
    void set_edge_start(const uint32_t start); // set start of edge of this node
private:
    uint32_t begin = 0; // index of string, where edge starts
    // end - index of the next char after the last char of edge
    // inf means that this edge last to the end of string
    uint32_t end = inf;
    uint32_t suffix_link = 0; // link to node with suffix which length less by 1
    vector<uint32_t> sons; // vector with sons
};

uint32_t Node::get_son(const char letter) const {
    assert(letter >= 0 and letter < alphabet_size);
    return sons[letter];
}

void Node::set_son(const char letter, const uint32_t son) {
    assert(letter >= 0 and letter < alphabet_size);
    sons[letter] = son;
}

uint32_t Node::get_suffix_link() const {
    return suffix_link;
}

void Node::set_suffix_link(const uint32_t link) {
    suffix_link = link;
}

uint32_t Node::get_edge_len(const uint32_t cur_position) const {
    return min(end, cur_position + 1) - begin;
}

uint32_t Node::get_edge_start() const {
    return begin;
}

uint32_t Node::get_edge_end() const {
    return end;
}

void Node::set_edge_start(const uint32_t start) {
    begin = start;
}

class Suffix_tree {
public:
    Suffix_tree(const string &text, const uint32_t lenght_first_string, 
                const uint32_t length_second_string);
    void construct(); // construct the suffix tree
    friend ostream& operator<<(ostream& out, const Suffix_tree& tree);
    // return -1 if there is no such substring, else return index in str,
    // where it starts
    int find_substring(const string &substring);
private:
    const string str; // string on which this tree will be built
    vector<Node> nodes; // suffix tree has not more than 2n nodes
    const uint32_t length_1; // length of first string
    const uint32_t length_2; // length of second string
    uint32_t node_counter = 0; // amount of nodes
    uint32_t need_add = 0; // amount of nodes-leaves that needs to add
    uint32_t need_suffix_link = 0; // new node without suffix link
    uint32_t active_node = 0; // node, that starts the edge, where we are
    uint32_t active_edge = 0; // index in str of char that start that edge
    uint32_t active_length = 0; // shows how far we are from active node

    // create node with fixed begin (end = inf by default)
    uint32_t make_node(const uint32_t begin, const uint32_t end);
    // connect node with node that need suffix link
    void add_suffix_link(const uint32_t node);
    // move down active point and retrun true, if it's possible; else false
    bool get_down(const uint32_t node, const uint32_t cur_position);
    // search every vertex, add the description quad to answer
    void dfs(const uint32_t node, const uint32_t parent, uint32_t &counter, 
             vector<uint32_t> &answer) const;
};

Suffix_tree::Suffix_tree(const string &text, const uint32_t lenght_first_string, 
                         const uint32_t length_second_string) :
                         str(text), 
                         length_1(lenght_first_string), 
                         length_2(length_second_string), 
                         nodes(vector<Node>(2 * text.length())) 
{
    nodes[node_counter++] = Node(0, 0);
};

void Suffix_tree::add_suffix_link(const uint32_t node) {
    if (need_suffix_link != 0) {
        nodes[need_suffix_link].set_suffix_link(node);
    }
    need_suffix_link = node;
}

bool Suffix_tree::get_down(const uint32_t node, const uint32_t cur_position) {
    const uint32_t length = nodes[node].get_edge_len(cur_position);
    // if it is possible to go down from active node by active edge to node and
    // decrease active length - go
    if (active_length >= length) {
        active_length -= length;
        active_edge += length;
        active_node = node;
        return true;
    }
    else {
        return false;
    }
}

uint32_t Suffix_tree::make_node(const uint32_t begin, 
                                const uint32_t end = inf) {
    const uint32_t index = node_counter++;
    nodes[index] = Node(begin, end);
    return index;
}

void Suffix_tree::construct() {
    for (uint32_t cur_index = 0; cur_index < str.length(); ++cur_index) {
        const char letter = str[cur_index];
        // index of node, that needs suffix link
        // root does not need suffix link
        need_suffix_link = 0;
        ++need_add;
        while (need_add > 0) {
            if (active_length == 0) {
                active_edge = cur_index;
            }
            const char edge = str[active_edge];
            // if there is no son by this edge
            if (nodes[active_node].get_son(edge) == 0) {
                const uint32_t next_node = make_node(cur_index);
                nodes[active_node].set_son(edge, next_node);
                add_suffix_link(active_node);
            }
            else {
                // go down from active node, check if we can continue from there
                const uint32_t next_node = nodes[active_node].get_son(edge);
                if (get_down(next_node, cur_index)) {
                    continue;
                }

                // if there is a continuation of edge by this letter:
                // increase active len, try to add suf link and go to next step
                const uint32_t start_index = nodes[next_node].get_edge_start();
                if (str[start_index + active_length] == letter) {
                    ++active_length;
                    add_suffix_link(active_node);
                    break;
                }

                // start - index in str, where starts active edge
                const uint32_t start = nodes[next_node].get_edge_start();
                const uint32_t end = start + active_length;
                // split active edge by active len, create split node
                const uint32_t split_node = make_node(start, end);
                // add split to sons of active edge
                nodes[active_node].set_son(edge, split_node);

                // create new node, that starts from current index of str
                const uint32_t new_node = make_node(cur_index);
                // add it to sons of split node
                nodes[split_node].set_son(letter, new_node);
                // start - new start index of edge between split and next nodes
                const uint32_t active_start = end;
                // set new start to next node
                nodes[next_node].set_edge_start(active_start);

                // get the char, that starts edge
                const char new_edge = str[nodes[next_node].get_edge_start()];
                // add new node as son to split node by new edge
                nodes[split_node].set_son(new_edge, next_node);
                // try to add suffix link
                add_suffix_link(split_node);
            }
            // decrease number of leaves, that we need to insert
            --need_add;

            // if active node is root, decrease active length
            // and turn to next edge
            // else continue from suffix link of active node
            if (active_node == 0 and active_length > 0) {
                --active_length;
                active_edge = cur_index + 1 - need_add;
            }
            else {
                active_node = nodes[active_node].get_suffix_link();
            }
        }
    }
}

void Suffix_tree::dfs(const uint32_t node, const uint32_t parent, 
                      uint32_t &counter, vector<uint32_t> &answer) const {
    // str num - number of string (0 or 1)
    const uint32_t str_num = nodes[node].get_edge_start() >= length_1 ? 1 : 0;
    answer.push_back(parent);
    answer.push_back(str_num);
    const uint32_t start = nodes[node].get_edge_start() - str_num * length_1;
    answer.push_back(start);
    if (nodes[node].get_edge_end() != inf) {
        const uint32_t end = nodes[node].get_edge_end() - str_num * length_1;
        answer.push_back(end);
    }
    else {
        if (str_num) {
            answer.push_back(length_2);
        }
        else {
            answer.push_back(length_1);
        }
    }
    const uint32_t cur_node_num = counter;
    for (char letter = 0; letter < alphabet_size; ++letter) {
        const uint32_t son = nodes[node].get_son(letter);
        if (son != 0) {
            ++counter;
            dfs(son, cur_node_num, counter, answer);
        }
    }
}

ostream& operator<<(ostream& out, const Suffix_tree& tree) {
    uint32_t counter = 0;
    // vector answer contains description of each vertex
    // each description presented by four numbers: parent, number of string, 
    // left and right borders of edge that lead to this vertex
    vector<uint32_t> answer;
    const uint32_t root = 0;
    const uint32_t num = 0;
    out << tree.node_counter << "\n";
    tree.dfs(root, num, counter, answer);
    // starts from 4 because answer should not contain root description
    for (uint32_t i = 4; i < answer.size(); i += 4) { // divide vector by quads
        // print the quad (description of one vertex)
        for (uint32_t j = 0; j < 4; ++j) {
            out << answer[i + j] << ' ';
        }
        out << "\n";
    }
    return out;
}

int Suffix_tree::find_substring(const string &substring) {
    uint32_t cur_node = 0;
    uint32_t cur_index = 0;
    uint32_t cur_edge_index = 0;
    uint32_t next_node = nodes[cur_node].get_son(substring[cur_index]);
    while (cur_index < substring.length() and 
           next_node != 0) {
        cur_edge_index = nodes[next_node].get_edge_start();
        ++cur_index;
        ++cur_edge_index;
        const uint32_t end = nodes[next_node].get_edge_end();
        while (cur_index < substring.length() and 
               cur_edge_index < end and 
               str[cur_edge_index] == substring[cur_index]) {
            ++cur_index;
            ++cur_edge_index;
        }
        if(cur_index == substring.length()) {
            break;
        }
        if(cur_edge_index != end) {
            break;
        }
        cur_node = next_node;
        next_node = nodes[cur_node].get_son(substring[cur_index]);
    }
    if (cur_index < substring.length()) {
        return -1;
    }
    else {
        return cur_edge_index - substring.length();
    }
}

int main() {
    string str_1, str_2;
    cin >> str_1 >> str_2;
    Suffix_tree tree(str_1 + str_2, str_1.length(), str_2.length());
    tree.construct();
    cout << tree;
    return 0;
}